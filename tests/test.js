// Import library for managing bitcoin cash cryptography.
import { hexToBin } from '@bitauth/libauth';

// Import the utility functions from this library.
import { replacer, reviver } from '../dist/module/index.js';

// Import the testing framework.
import test from 'ava';

// Define the testing source data.
const emptyData = hexToBin('');
const sourceData = hexToBin('00112233445566778899aabbccddeeff').buffer;
const bigInteger = BigInt('9007199254740991');

// Create views of the source data.
const dataViews =
{
	Int8Array: new Int8Array(sourceData),
	Int16Array: new Int16Array(sourceData),
	Int32Array: new Int32Array(sourceData),

	Uint8Array: new Uint8Array(sourceData),
	Uint16Array: new Uint16Array(sourceData),
	Uint32Array: new Uint32Array(sourceData),

	Uint8ClampedArray: new Uint8ClampedArray(sourceData),

	Float32Array: new Float32Array(sourceData),
	Float64Array: new Float64Array(sourceData),

	BigInt64Array: new BigInt64Array(sourceData),
	BigUint64Array: new BigUint64Array(sourceData),
};

// Declare a function that converts to JSON and back, then verifies integrity.
const conversionTest = function(input, t)
{
	// Convert to JSON, then parse it back, using the functions from this library.
	const stringified = JSON.stringify(input, replacer);
	const output = JSON.parse(stringified, reviver);

	// Verify that the input and output is the same.
	t.deepEqual(output, input, 'Data should not change after passing through JSON stringification.');
};

// Test the empty-data edgecase.
test('Convert an empty Uint8Array through JSON', conversionTest.bind(null, emptyData));

// Test the big integer data type.
test('Convert BigInteger through JSON', conversionTest.bind(null, bigInteger));

// Test the array buffer.
test('Convert ArrayBuffer through JSON', conversionTest.bind(null, sourceData));

// For each data view (typed array buffer).
for(const viewIndex in dataViews)
{
	// Test the data view conversion.
	test(`Convert ${viewIndex} through JSON.`, conversionTest.bind(null, dataViews[viewIndex]));
}
