// Import package.json to reduce name repetition.
// eslint-disable-next-line import/extensions
import pkg from './package.json';
import dts from 'rollup-plugin-dts';

// Configure what to export.
export default
[
	{
		// Source file
		input:
		[
			'lib/index.js',
		],

		// Libraries that we do not include, but instead load dynamically.
		external:
		[
			...Object.keys(pkg.dependencies || {}),
			...Object.keys(pkg.peerDependencies || {}),
		],
		// The output format and files names.
		output:
		[
			{
				dir: pkg.main,
				format: 'cjs',
			},
			{
				dir: pkg.module,
				format: 'esm',
				preserveModules: true,
			},
		],
	},
	{
		input: 'dist/index.d.ts',
		output:
		{
			file: 'dist/typings/index.d.ts',
			format: 'es',
		},
		plugins: [ dts() ],
	},
];
